(* ========================================================================= *)
(* Isometries and reflections.                                               *)
(*                                                                           *)
(* Copyright (c) 2014 Marco Maggesi                                          *)
(* ========================================================================= *)

let CUT_TAC : term -> tactic =
  let th = MESON [] `(p ==> q) /\ p ==> q`
  and ptm = `p:bool` in
  fun tm -> MATCH_MP_TAC (INST [tm,ptm] th) THEN CONJ_TAC;;

let RULE_HYP_TAC s rule = REMOVE_THEN s (LABEL_TAC s o rule);;

(* ------------------------------------------------------------------------- *)
(* Isometry.                                                                 *)
(* ------------------------------------------------------------------------- *)

let isometry = new_definition
  `!f:real^N->real^M. isometry f <=> !x y. dist(f x, f y) = dist(x,y)`;;

let ISOMETRY_I = prove
 (`isometry (I:real^N->real^N)`,
  REWRITE_TAC[isometry; I_THM]);;

let ISOMETRY_o = prove
 (`!f:real^M->real^P g:real^N->real^M.
     isometry f /\ isometry g ==> isometry (f o g)`,
  SIMP_TAC[isometry; o_THM]);;

let ISOMETRY_TRANSLATION = prove
 (`!a:real^N. isometry (\x. a + x)`,
  REWRITE_TAC[isometry] THEN NORM_ARITH_TAC);;

let ISOMETRY_BETWEEN = prove
 (`!f:real^N->real^M a b c.
     isometry f /\ between b (a,c) ==> between (f b) (f a, f c)`,
  REWRITE_TAC[isometry; between] THEN MESON_TAC[]);;

let ISOMETRY_MIDPOINT = prove
 (`!f:real^N->real^N a b.
     isometry f ==> f(midpoint(a,b)) = midpoint(f a, f b)`,
  SIMP_TAC[isometry; MIDPOINT_BETWEEN; DIST_MIDPOINT; between] THEN
  REAL_ARITH_TAC);;

let ISOMETRY_EQ_AFFINITY_ORTHOGONAL = prove
 (`!f:real^N->real^N.
     isometry f <=>
     ?h. orthogonal_transformation h /\ (!x. f x = f (vec 0) + h x)`,
  GEN_TAC THEN REWRITE_TAC[isometry] THEN EQ_TAC THENL
  [INTRO_TAC "isom" THEN
   USE_THEN "isom"
     (DESTRUCT_TAC "@h. lin aff" o MATCH_MP ISOMETRY_IMP_AFFINITY) THEN
   EXISTS_TAC `h:real^N->real^N` THEN
   SUBGOAL_THEN `!x:real^N. h x:real^N = f x - f (vec 0)` (LABEL_TAC "h") THENL
   [GEN_TAC THEN REMOVE_THEN "aff" (MP_TAC o SPEC `x:real^N`) THEN
    VECTOR_ARITH_TAC;
    ALL_TAC] THEN
   CONJ_TAC THENL
   [REWRITE_TAC[ORTHOGONAL_TRANSFORMATION_ISOMETRY] THEN CONJ_TAC THENL
    [HYP MESON_TAC "lin" [LINEAR_0];
     REPEAT GEN_TAC THEN
     TRANS_TAC EQ_TRANS `dist (f (x:real^N):real^N, f y)` THEN CONJ_TAC THENL
     [USE_THEN "h" (fun th -> REWRITE_TAC[th]) THEN NORM_ARITH_TAC;
      USE_THEN "isom" MATCH_ACCEPT_TAC]];
    USE_THEN "h" (fun th -> REWRITE_TAC[th]) THEN VECTOR_ARITH_TAC];
   REWRITE_TAC[ORTHOGONAL_TRANSFORMATION_ISOMETRY] THEN
   INTRO_TAC "@h. (h0 isom) aff; !x y" THEN HYP ONCE_REWRITE_TAC "aff" [] THEN
   TRANS_TAC EQ_TRANS `dist(h (x:real^N):real^N, h y)` THEN
   CONJ_TAC THENL
   [NORM_ARITH_TAC; HYP REWRITE_TAC "isom" []]]);;

(* ------------------------------------------------------------------------- *)
(* Set of fixed points.                                                      *)
(* ------------------------------------------------------------------------- *)

let FIX = new_definition
  `FIX f = {x:A | f x = x}`;;

let IN_FIX = prove
 (`!f x:A. x IN FIX f <=> f x = x`,
  REWRITE_TAC[EXTENSION; FIX; IN_ELIM_THM]);;

(* ------------------------------------------------------------------------- *)
(* Reflection of a point wrt a set.                                          *)
(* ------------------------------------------------------------------------- *)

let reflect = new_definition
  `reflect s x : real^N = &2 % project s x - x`;;

let REFLECT_TRANSLATION = prove
 (`!a s x:real^N. reflect (IMAGE (\x. a + x) s) (a + x) = a + reflect s x`,
  REWRITE_TAC[reflect; PROJECT_TRANSLATION] THEN VECTOR_ARITH_TAC);;

add_translation_invariants [REFLECT_TRANSLATION];;

let REFLECT_FIXPOINT = prove
 (`!s:real^N->bool. x IN s ==> reflect s x = x`,
  INTRO_TAC "!s; x" THEN REWRITE_TAC[reflect] THEN
  CUT_TAC `project s x = x:real^N` THENL
  [VECTOR_ARITH_TAC; ASM_SIMP_TAC[PROJECT_SELF]]);;

let REFLECT_AFFINE_FIXPOINT = prove
 (`!s:real^N->bool.
     affine s /\ ~(s = {}) ==> (!x. reflect s x = x <=> x IN s)`,
  INTRO_TAC "!s; aff ne; !x" THEN
  CUT_TAC `reflect s x = x:real^N ==> x IN s` THENL
  [ASM_MESON_TAC[REFLECT_FIXPOINT]; ALL_TAC] THEN
  REWRITE_TAC[reflect; VECTOR_ARITH
    `&2 % project s x - x = x:real^N <=> project s x = x`] THEN
  ASM_MESON_TAC[PROJECT_IN_SET; CLOSED_AFFINE]);;

let FIX_REFLECT = prove
 (`!s:real^N->bool. affine s /\ ~(s = {})==> FIX (reflect s) = s`,
  REWRITE_TAC[FIX; EXTENSION; IN_ELIM_THM] THEN
  MESON_TAC[REFLECT_AFFINE_FIXPOINT; REFLECT_FIXPOINT]);;

(* ------------------------------------------------------------------------- *)
(* Reflection wrt a vector subspace.                                         *)
(* ------------------------------------------------------------------------- *)

let REFLECT_SUBSPACE_EXPAND = prove
 (`!s b b' x.
     subspace s /\ span b = s /\ span (b UNION b') = (:real^N) /\
     pairwise orthogonal (b UNION b') /\ DISJOINT b b' /\
     (!v. v IN b ==> norm v = &1) /\ (!v. v IN b' ==> norm v = &1)
     ==> reflect s x =
         vsum b (\v. (v dot x) % v) - vsum b' (\v. (v dot x) % v)`,
  INTRO_TAC "!s b b' x; subp span total orth disj unit unit'" THEN
  SUBGOAL_THEN
    `pairwise orthogonal (b:real^N->bool) /\
     pairwise orthogonal (b':real^N->bool)`
    (DESTRUCT_TAC "orth1 orth2") THENL
  [REMOVE_THEN "orth" MP_TAC THEN SIMP_TAC[pairwise; orthogonal; IN_UNION];
   ALL_TAC] THEN
  SUBGOAL_THEN `FINITE (b:real^N->bool) /\ FINITE (b':real^N->bool)`
    (DESTRUCT_TAC "fin1 fin2") THENL
  [HYP SIMP_TAC "orth1 orth2" [PAIRWISE_ORTHOGONAL_IMP_FINITE]; ALL_TAC] THEN
  REWRITE_TAC[reflect] THEN IMP_REWRITE_TAC[PROJECT_SUBSPACE_EXPAND] THEN
  MATCH_MP_TAC
    (VECTOR_ARITH `!u w x:real^N. u + w = x ==> &2 % u - x = u - w`) THEN
  IMP_REWRITE_TAC[GSYM VSUM_UNION] THEN
  MATCH_MP_TAC ORTHONORMAL_BASIS_EXPAND THEN
  HYP REWRITE_TAC "total orth" [IN_UNIV] THEN
  REWRITE_TAC[IN_UNION] THEN HYP MESON_TAC "unit unit'" []);;

let LINEAR_REFLECT = prove
 (`!s:real^N->bool. subspace s ==> linear (reflect s)`,
  INTRO_TAC "!s; sub" THEN REWRITE_TAC[linear; reflect] THEN
  ASM_SIMP_TAC[LINEAR_PROJECT; LINEAR_ADD; LINEAR_CMUL] THEN
  VECTOR_ARITH_TAC);;

let ORTHOGONAL_TRANSFORMATION_REFLECT = prove
 (`!s:real^N->bool. subspace s ==> orthogonal_transformation (reflect s)`,
  SIMP_TAC[orthogonal_transformation; LINEAR_REFLECT] THEN
  INTRO_TAC "!s; subsp; !v w" THEN
  USE_THEN "subsp" (MP_TAC o MATCH_MP ORTHONORMAL_BASIS_SUBSPACE) THEN
  INTRO_TAC "@b. sub orth unit indep dim span" THEN
  HYP (MP_TAC o SPEC `(:real^N)` o MATCH_MP ORTHONORMAL_EXTENSION o
    end_itlist CONJ) "orth unit" [] THEN
  REWRITE_TAC[UNION_UNIV; SPAN_UNIV] THEN ONCE_REWRITE_TAC[DISJOINT_SYM] THEN
  INTRO_TAC "@b'. disj orth' unit' span'" THEN
  SUBGOAL_THEN
    `!v. reflect s v =
         vsum b (\x. (x dot v) % x) - vsum b' (\x. (x dot v) % x):real^N`
    (fun th -> REWRITE_TAC[th]) THENL
  [GEN_TAC THEN MATCH_MP_TAC REFLECT_SUBSPACE_EXPAND THEN ASM_REWRITE_TAC[];
   ALL_TAC] THEN
  SUBGOAL_THEN `FINITE (b:real^N->bool) /\ FINITE (b':real^N->bool)`
    (DESTRUCT_TAC "fin fin'") THENL
  [CONJ_TAC THEN MATCH_MP_TAC PAIRWISE_ORTHOGONAL_IMP_FINITE THEN
   REMOVE_THEN "orth'" MP_TAC THEN
   REWRITE_TAC[pairwise; orthogonal; IN_UNION] THEN MESON_TAC[];
   ALL_TAC] THEN
  SUBGOAL_THEN
    `!x:real^N. x = vsum b (\u. (u dot x) % u) + vsum b' (\u. (u dot x) % u)`
    (fun th -> GEN_REWRITE_TAC (RAND_CONV o ONCE_DEPTH_CONV) [th]) THENL
  [GEN_TAC THEN IMP_REWRITE_TAC[GSYM VSUM_UNION] THEN
   MATCH_MP_TAC (GSYM ORTHONORMAL_BASIS_EXPAND) THEN
   HYP REWRITE_TAC "orth' span'" [IN_UNION; IN_UNIV] THEN
   HYP MESON_TAC "unit unit'" [];
   ALL_TAC] THEN
  REWRITE_TAC[DOT_LADD; DOT_RADD; DOT_LSUB; DOT_RSUB] THEN
  ASM_SIMP_TAC[PAIRWISE_ORTHOGONAL_IMP_FINITE; DOT_LSUM; DOT_RSUM] THEN
  REWRITE_TAC[DOT_LMUL; DOT_RMUL] THEN
  SUBGOAL_THEN
    `sum b (\x. sum b' (\x'. (x dot v) * (x' dot w) * (x dot x'))) = &0 /\
     sum b' (\x:real^N. sum b (\x'. (x dot v) * (x' dot w) * (x dot x'))) = &0`
    (fun th -> REWRITE_TAC[th]) THENL
  [IMP_REWRITE_TAC[SUM_EQ_0] THEN CONJ_TAC THEN
   GEN_TAC THEN INTRO_TAC "xhp" THEN X_GEN_TAC `y:real^N` THEN
   INTRO_TAC "yhp" THEN REMOVE_THEN "orth'" MP_TAC THEN
   REWRITE_TAC[pairwise; orthogonal; IN_UNION; REAL_ENTIRE] THEN
   REMOVE_THEN "disj" MP_TAC THEN REWRITE_TAC[IN_DISJOINT] THEN
   HYP MESON_TAC "xhp yhp" [REAL_MUL_RZERO; REAL_MUL_LZERO];
   ALL_TAC] THEN
  REAL_ARITH_TAC);;

let ISOMETRY_TRANSLATION_INVARIANT = prove
 (`!a f:real^M->real^N. isometry (\x. a + f x) <=> isometry f`,
  REWRITE_TAC[isometry] THEN
  MESON_TAC[NORM_ARITH `!x y:real^N. dist(a+x,a+y) = dist(x,y)`]);;

let TRANSLATION_ISOMETRY_INVARIANT = prove
 (`!a f:real^M->real^N. isometry (\x. f (a + x)) <=> isometry f`,
  REWRITE_TAC[isometry] THEN REPEAT GEN_TAC THEN EQ_TAC THENL
  [REPEAT STRIP_TAC THEN
   POP_ASSUM (MP_TAC o SPECL[`x - a:real^M`; `y - a:real^M`]) THEN
   REWRITE_TAC[VECTOR_ARITH `a + x - a = x:real^M`] THEN
   DISCH_THEN SUBST1_TAC THEN NORM_ARITH_TAC;
   REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[] THEN NORM_ARITH_TAC]);;

let REFLECT_TRANSLATION_ALT = prove
 (`!a:real^N s. reflect (IMAGE (\x. a + x) s) = (\x. a + reflect s (--a + x))`,
  GEN_REWRITE_TAC I
    [MESON [VECTOR_NEG_NEG] `!P. (!a:real^N. P a) <=> (!a. P (--a))`] THEN
  GEOM_ORIGIN_TAC `a:real^N` THEN
  REWRITE_TAC[FUN_EQ_THM; VECTOR_ADD_RID; VECTOR_NEG_NEG; REFLECT_TRANSLATION;
              GSYM IMAGE_o; o_DEF; IMAGE_ID;
              VECTOR_ARITH `!v:real^N. --a + a + v = v`]);;

let ISOMETRY_EQ_ORTHOGONAL_TRANSFORMATION = prove
 (`!f:real^N->real^N. f (vec 0) = vec 0
                      ==> (isometry f <=> orthogonal_transformation f)`,
  REPEAT STRIP_TAC THEN
  ASM_REWRITE_TAC[ISOMETRY_EQ_AFFINITY_ORTHOGONAL; VECTOR_ADD_LID] THEN
  REWRITE_TAC[GSYM FUN_EQ_THM] THEN
  MESON_TAC[]);;

let ISOMETRY_REFLECT = prove
 (`!s:real^N->bool. affine s /\ ~(s = {}) ==> isometry (reflect s)`,
  SUBGOAL_THEN `!s a:real^N. affine s /\ a IN s ==> isometry (reflect s)`
    (fun th -> MESON_TAC[th; MEMBER_NOT_EMPTY]) THEN
  GEOM_ORIGIN_TAC `a:real^N` THEN
  REWRITE_TAC[REFLECT_TRANSLATION_ALT; ISOMETRY_TRANSLATION_INVARIANT;
              TRANSLATION_ISOMETRY_INVARIANT] THEN
  REWRITE_TAC [MESON [AFFINE_IMP_SUBSPACE; SUBSPACE_0; SUBSPACE_IMP_AFFINE]
                     `affine s /\ vec 0:real^N IN s <=> subspace s`] THEN
  INTRO_TAC "!s; subsp" THEN
  SUBGOAL_THEN `reflect s (vec 0) = vec 0:real^N` ASSUME_TAC THENL
  [MATCH_MP_TAC (ISPEC `reflect (s:real^N->bool)` LINEAR_0) THEN
   ASM_SIMP_TAC[LINEAR_REFLECT] ;
   ALL_TAC] THEN
  ASM_SIMP_TAC[ISOMETRY_EQ_ORTHOGONAL_TRANSFORMATION;
               ORTHOGONAL_TRANSFORMATION_REFLECT]);;

let PROJECT_IDEMP = prove
 (`!s x:real^N.
     closed s /\ ~(s = {}) ==> project s (project s x) = project s x`,
  SIMP_TAC[PROJECT_SELF; PROJECT_IN_SET]);;

let PROJECT_REFLECT = prove
 (`!s x:real^N.
     affine s /\ ~(s = {}) ==> project s (reflect s x) = project s x`,
  SUBGOAL_THEN
    `!s x a:real^N.
       affine s /\ a IN s ==> project s (reflect s x) = project s x`
    (fun th -> SET_TAC[th]) THEN
  GEOM_ORIGIN_TAC `a:real^N` THEN REWRITE_TAC[GSYM SUBSPACE_EQ_AFFINE] THEN
  INTRO_TAC "!s x; subsp" THEN
  ASM_SIMP_TAC[reflect; LINEAR_PROJECT; LINEAR_SUB; LINEAR_CMUL] THEN
  ASM_SIMP_TAC[PROJECT_IDEMP; CLOSED_SUBSPACE; SUBSPACE_IMP_NONEMPTY] THEN
  VECTOR_ARITH_TAC);;

let REFLECT_INVOLUTION = prove
 (`!s x:real^N. affine s /\ ~(s = {}) ==> reflect s (reflect s x) = x`,
  REPEAT STRIP_TAC THEN GEN_REWRITE_TAC LAND_CONV [reflect] THEN
  ASM_SIMP_TAC[PROJECT_REFLECT] THEN REWRITE_TAC[reflect] THEN
  VECTOR_ARITH_TAC);;

let PROJECT_SUBSPACE_UNIQUE = prove
 (`!s x y:real^N. subspace s /\ y IN s /\ x-y IN perp s ==> project s x = y`,
  INTRO_TAC "!s x y; subsp y diff" THEN
  MATCH_MP_TAC EQ_SYM THEN MATCH_MP_TAC PROJECT_UNIQUE THEN
  HYP SIMP_TAC "y subsp" [CLOSED_SUBSPACE; SUBSPACE_IMP_CONVEX] THEN
  MATCH_MP_TAC ORTHOGONAL_ANY_CLOSEST_POINT THEN
  HYP REWRITE_TAC "y" [] THEN REMOVE_THEN "diff" MP_TAC THEN
  REWRITE_TAC[IN_PERP; orthogonal; DOT_LSUB; DOT_RSUB] THEN
  INTRO_TAC "hp; !z; z" THEN
  USE_THEN "hp" (MP_TAC o C MATCH_MP (ASSUME `z:real^N IN s`)) THEN
  USE_THEN "hp" (MP_TAC o C MATCH_MP (ASSUME `y:real^N IN s`)) THEN
  REWRITE_TAC[SUB_REFL] THEN REAL_ARITH_TAC);;

let PROJECT_AFFINE_UNIQUE = prove
 (`!s x y:real^N.
     affine s /\ y IN s /\ x-y IN perp (direction s) ==> project s x = y`,
  GEOM_ORIGIN_TAC `y:real^N` THEN REWRITE_TAC[VECTOR_SUB_RZERO] THEN
  INTRO_TAC "!s x; aff orig x" THEN
  CLAIM_TAC "subsp" `subspace (s:real^N->bool)` THENL
  [ASM_SIMP_TAC[AFFINE_IMP_SUBSPACE]; ALL_TAC] THEN
  MATCH_MP_TAC PROJECT_SUBSPACE_UNIQUE THEN
  ASM_SIMP_TAC[SUBSPACE_PERP; SUBSPACE_0; VECTOR_SUB_RZERO] THEN
  HYP (RULE_HYP_TAC "x" o SIMP_RULE) "subsp" [DIRECTION_SUBSPACE] THEN
  USE_THEN "x" ACCEPT_TAC);;

let REFLECT_SUBSPACE_UNIQUE = prove
 (`!s x y:real^N.
     subspace s /\ midpoint(x,y) IN s /\ x-y IN perp s ==> reflect s x = y`,
  INTRO_TAC "!s x y; subsp mid diff" THEN REWRITE_TAC[reflect] THEN
  CUT_TAC `project s x = midpoint(x,y):real^N` THENL
  [REWRITE_TAC[midpoint] THEN VECTOR_ARITH_TAC; ALL_TAC] THEN
  MATCH_MP_TAC PROJECT_SUBSPACE_UNIQUE THEN ASM_REWRITE_TAC[] THEN
  SUBGOAL_THEN `x - midpoint(x,y) = inv(&2) % (x - y):real^N` SUBST1_TAC THENL
  [REWRITE_TAC[midpoint] THEN VECTOR_ARITH_TAC; ALL_TAC] THEN
  HYP SIMP_TAC "diff" [SUBSPACE_PERP; SUBSPACE_MUL]);;

let REFLECT_AFFINE_UNIQUE = prove
 (`!s x y:real^N.
     affine s /\ midpoint(x,y) IN s /\ x-y IN perp (direction s)
     ==> reflect s x = y`,
  SUBGOAL_THEN
    `!s x y a:real^N.
       affine s /\ a = midpoint(x,y) /\ a IN s /\ x-y IN perp (direction s)
       ==> reflect s x = y`
    (fun th -> MESON_TAC[th]) THEN
  GEOM_ORIGIN_TAC `a:real^N` THEN REPEAT STRIP_TAC THEN
  MATCH_MP_TAC REFLECT_SUBSPACE_UNIQUE THEN
  CLAIM_TAC "subsp" `subspace (s:real^N->bool)` THENL
  [ASM_SIMP_TAC[AFFINE_IMP_SUBSPACE]; ALL_TAC] THEN
  ASM_MESON_TAC[SUBSPACE_0; DIRECTION_SUBSPACE]);;

let REFLECT_ALONG_EQ_REFLECT = prove
 (`!v:real^N. ~(v = vec 0) ==> reflect_along v = reflect {w | orthogonal v w}`,
  REWRITE_TAC[FUN_EQ_THM] THEN INTRO_TAC "!v; vnz; !x" THEN
  MATCH_MP_TAC EQ_SYM THEN MATCH_MP_TAC REFLECT_SUBSPACE_UNIQUE THEN
  SIMP_TAC[SUBSPACE_IMP_AFFINE; SUBSPACE_ORTHOGONAL_TO_VECTOR;
           DIRECTION_SUBSPACE] THEN
  REWRITE_TAC[IN_PERP; IN_ELIM_THM; orthogonal; midpoint; reflect_along] THEN
  CONJ_TAC THENL
  [REWRITE_TAC[VECTOR_ARITH
     `inv (&2) % (x + x - (&2 * (x dot v) / (v dot v)) % v) =
      x - (x dot v) / (v dot v) % v:real^N`] THEN
   REWRITE_TAC[DOT_RSUB; DOT_RMUL; DOT_SYM] THEN
   CUT_TAC `~(v dot v:real^N = &0)` THENL
   [CONV_TAC REAL_FIELD; ASM_REWRITE_TAC[DOT_EQ_0]];
   REWRITE_TAC[VECTOR_ARITH
     `x - (x - (&2 * (x dot v) / (v dot v)) % v) =
      (&2 * (x dot v) / (v dot v)) % v:real^N`] THEN
   SIMP_TAC[DOT_RMUL; DOT_SYM; REAL_MUL_RZERO]]);;
